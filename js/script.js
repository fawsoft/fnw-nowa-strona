$(document).ready(function(){ 

	setLayout();

	$('.solutions').parallax({imageSrc: 'images/tlo_1.jpg', bleed:100});
	$('.clients').parallax({imageSrc: 'images/tlo_1.jpg', bleed:40});
	$('.our-team').parallax({imageSrc: 'images/tlo_3.jpg'});
	$('.careers').parallax({imageSrc: 'images/tlo_2.jpg'});

	$('.tile').on('mouseover', function(){
		
	});
	
	$('.ctile')
		.mouseenter(function(){
			$(this).find('.in').animate({top: '-200px'}); 
		})
		.mouseleave(function(){
			$(this).find('.in').animate({top: '0px'});
		});
	
	$('.member')
		.mouseenter(function(){
			$(this).find('.img').animate({width: '+=20px', height: '+=20px', top: '-=10px'}, 100); 
		})
		.mouseleave(function(){
			$(this).find('.img').animate({width: '-=20px', height: '-=20px', top: '+=10px'}, 100);
		});
	
	$('.btn-black').click(function(e){
		e.preventDefault();
		$('body').scrollTo(0, 800);
	});
	
	$('.btn-apply').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.get-in-touch', 800, {offset: -62});
	});
	
	$('a.home').click(function(e){
		e.preventDefault();
		$('body').scrollTo(0, 800);
	});
	
	$('a.mabout').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.about', 800, {offset: -62});
	});
	
	$('a.msolutions').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.solutions', 800, {offset: -62});
	});
	
	$('a.mcase-study').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.case-study', 800, {offset: -62});
	});
	
	$('a.mclients').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.clients', 800, {offset: -62});
	});
	
	$('a.mour-team').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.our-team', 800, {offset: -62});
	});
	
	$('a.mcareers').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.careers', 800, {offset: -62});
	});
	
	$('a.mcontact').click(function(e){
		e.preventDefault();
		$('body').scrollTo('.get-in-touch', 800, {offset: -62});
	});
	
});

$(window).scroll(function(){ 
	if($(this).scrollTop() > 400) { 
		$('.menu-bar').css({backgroundColor: 'white'});
		$('.menu li a').css({color: 'black'});
		$('.menu-bar').css({paddingTop: '10px'});
		$('.menu-bar').css({paddingBottom: '5px'});
		$('.menu-bar').css({borderBottom: '1px solid lightgray'});
		$('header .logo').css({background: 'url(images/logo_black.png)'});
		$('.menu .facebook').css({background: 'url(images/fb_black.png)'});
		$('.menu .twitter').css({background: 'url(images/tw_black.png)'});
		$('.menu .linkedin').css({background: 'url(images/ln_black.png)'});
	}
	if($(this).scrollTop() <= 400) { 
		$('.menu-bar').css({backgroundColor: 'transparent'});
		$('.menu li a').css({color: 'white'});
		$('.menu-bar').css({paddingTop: '10px'});
		$('.menu-bar').css({paddingBottom: '5px'});
		$('.menu-bar').css({borderBottom: '0px solid gray'});
		$('header .logo').css({background: 'url(images/logo_white.png)'});
		$('.menu .facebook').css({background: 'url(images/fb_white.png)'});
		$('.menu .twitter').css({background: 'url(images/tw_white.png)'});
		$('.menu .linkedin').css({background: 'url(images/ln_white.png)'});
	}
});

$(window).resize(function(){
	setLayout();
});

function setLayout(){
	var ww = $(window).width();
	var wh = $(window).height();
	$('.100vh').height(wh);
	$('.fullh').height(wh-62);
	$('.60vh').height(Math.floor(wh*0.6));
}